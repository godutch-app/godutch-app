run:
	@npm start

start: docker-build
	@docker run --rm -p 4200:80 godutch-app

docker-build:
	@docker build -t godutch-app .

.PHONY: run start docker-build