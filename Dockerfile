FROM node:16.13 as builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM nginx:stable-alpine
COPY --from=builder /app/dist/godutch-app /usr/share/nginx/html
EXPOSE 80