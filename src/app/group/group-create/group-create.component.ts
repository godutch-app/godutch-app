import { GroupService } from './../../core/services/group.service';
import { Group } from './../../core/models/Group';
import { Component, OnInit } from '@angular/core';
import * as uuid from 'uuid';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-group-create',
  templateUrl: './group-create.component.html',
  styleUrls: ['./group-create.component.scss'],
})
export class GroupCreateComponent implements OnInit {
  group: Group = new Group();
  addMember: boolean = false;
  memberId: string;
  error: string = '';

  constructor(
    private route: ActivatedRoute,
    private groupService: GroupService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.addMember = !!params.addMember;
      this.memberId = params.addMember;
    });
  }

  onSubmit(): void {
    const id = uuid.v4();
    this.groupService.create({ ...this.group, id }).subscribe(
      () => {
        if (this.addMember) {
          this.groupService.addMember(id, this.memberId).subscribe(() => {
            this.router.navigate([`/groups/${id}`], {
              queryParams: { referral: this.memberId },
            });
          });
        } else {
          this.router.navigate([`/groups/${id}`]);
        }
      },
      (err) => {
        this.error =
          err.errors && err.errors.length ? err.errors[0] : err.message;
      }
    );
  }
}
