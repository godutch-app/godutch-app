import { UserService } from './../../core/services/user.service';
import { GroupService } from './../../core/services/group.service';
import { User } from './../../core/models/User';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  faUserTimes,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-group-detail-members',
  templateUrl: './group-detail-members.component.html',
  styleUrls: ['./group-detail-members.component.scss'],
})
export class GroupDetailMembersComponent implements OnInit {
  faUserTimes: IconDefinition = faUserTimes;

  @Input() groupId: string;
  @Input() members: User[];
  @Output() refreshGroup = new EventEmitter<void>();
  newMember: string = '';
  error: string = '';

  constructor(
    private groupService: GroupService,
    private userService: UserService
  ) {}

  ngOnInit(): void {}

  addMember(): void {
    this.error = '';
    this.userService.findByEmail(this.newMember).subscribe(
      (user) => {
        this.groupService.addMember(this.groupId, user.id).subscribe(() => {
          this.refreshGroup.emit()
          this.newMember = '';
        });
      },
      (err) => {
        this.error =
          err.errors && err.errors.length ? err.errors[0] : err.message;
      }
    );
  }

  removeMember(e: PointerEvent, member: User): void {
    e.preventDefault();
    this.groupService
      .removeMember(this.groupId, member.id)
      .subscribe(() => this.refreshGroup.emit());
  }
}
