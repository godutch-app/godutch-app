import { ExpenseService } from './../../core/services/expense.service';
import { Expense } from './../../core/models/Expense';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  faTrashAlt,
  faEdit,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-group-detail-expenses',
  templateUrl: './group-detail-expenses.component.html',
  styleUrls: ['./group-detail-expenses.component.scss'],
})
export class GroupDetailExpensesComponent implements OnInit {
  faTrashAlt: IconDefinition = faTrashAlt;
  faEdit: IconDefinition = faEdit;

  @Input() groupId: string;
  @Input() expenses: Expense[];
  @Output() refreshExpenses = new EventEmitter<void>();
  @Output() refreshBalance = new EventEmitter<void>();

  constructor(private expenseService: ExpenseService) {}

  ngOnInit(): void {}

  getExpenses(): void {
    this.expenseService.findByGroup(this.groupId).subscribe((expenses) => {
      this.expenses = expenses;
    });
  }

  deleteExpense(e: PointerEvent, expense: Expense): void {
    e.preventDefault();
    this.expenseService.delete(expense.id).subscribe(() => {
      this.refreshExpenses.emit();
      this.refreshBalance.emit();
    });
  }
}
