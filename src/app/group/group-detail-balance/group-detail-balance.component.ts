import { Balance } from './../../core/models/Balance';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-group-detail-balance',
  templateUrl: './group-detail-balance.component.html',
  styleUrls: ['./group-detail-balance.component.scss'],
})
export class GroupDetailBalanceComponent implements OnInit {
  @Input() balance: Balance;
  @Input() groupHasExpenses: boolean;
  @Input() groupHasMoreThanOneMember: boolean;

  constructor() {}

  ngOnInit(): void {}

  parseAmount(num: number) {
    return Math.abs(num).toFixed(2);
  }
}
