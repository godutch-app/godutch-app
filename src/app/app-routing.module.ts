import { ExpenseEditComponent } from './expense/expense-edit/expense-edit.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// COMPONENTS
import { UserLoginComponent } from './user/user-login/user-login.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { GroupCreateComponent } from './group/group-create/group-create.component';
import { GroupDetailComponent } from './group/group-detail/group-detail.component';
import { GroupEditComponent } from './group/group-edit/group-edit.component';
import { ExpenseCreateComponent } from './expense/expense-create/expense-create.component';

const routes: Routes = [
  {path: "", component: UserLoginComponent},
  {path: "users/create", component: UserCreateComponent},
  {path: "users/:id", component: UserDetailComponent},
  {path: "users/edit/:id", component: UserEditComponent},
  {path: "groups/create", component: GroupCreateComponent},
  {path: "groups/:id", component: GroupDetailComponent},
  {path: "groups/edit/:id", component: GroupEditComponent},
  {path: "expenses/create", component: ExpenseCreateComponent},
  {path: "expenses/edit/:id", component: ExpenseEditComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
