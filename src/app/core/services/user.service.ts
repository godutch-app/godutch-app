import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor (
    private apiService: ApiService
  ) {}

  private USERS_PATH: String = "users"
  private EMAIL_PATH: String = "email"

  findById(id: string): Observable<User> {
    return this.apiService.get(`${this.USERS_PATH}/${id}`)
  }

  findByEmail(email: string): Observable<User> {
    return this.apiService.get(`${this.USERS_PATH}/${this.EMAIL_PATH}/${email}`)
  }

  create(user: User): Observable<void> {
    return this.apiService.put(`${this.USERS_PATH}`, user)
  }

  update(user: User): Observable<void> {
    return this.apiService.patch(`${this.USERS_PATH}`, user)
  }

  delete(id: string): Observable<void> {
    return this.apiService.delete(`${this.USERS_PATH}/${id}`)
  }
}
