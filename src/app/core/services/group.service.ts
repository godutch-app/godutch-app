import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Group } from '../models/Group';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GroupService {
  constructor(private apiService: ApiService) {}

  private GROUPS_PATH: String = 'groups';
  private MEMBER_PATH: String = 'member';

  findById(id: string): Observable<Group> {
    return this.apiService.get(`${this.GROUPS_PATH}/${id}`);
  }

  findByMember(memberId: string): Observable<Group[]> {
    return this.apiService
      .get(`${this.GROUPS_PATH}/${this.MEMBER_PATH}/${memberId}`)
      .pipe(
        map((groups) =>
          groups.sort((a, b) =>
            a.name.localeCompare(b.name, 'es', { ignorePunctuation: true })
          )
        )
      );
  }

  create(group: Group): Observable<void> {
    return this.apiService.put(`${this.GROUPS_PATH}`, group);
  }

  update(group: Group): Observable<void> {
    return this.apiService.patch(`${this.GROUPS_PATH}`, group);
  }

  addMember(groupId: string, userId: string): Observable<void> {
    return this.apiService.patch(
      `${this.GROUPS_PATH}/${this.MEMBER_PATH}/add`,
      { groupId, userId }
    );
  }

  removeMember(groupId: string, userId: string): Observable<void> {
    return this.apiService.patch(
      `${this.GROUPS_PATH}/${this.MEMBER_PATH}/remove`,
      { groupId, userId }
    );
  }

  delete(id: string): Observable<void> {
    return this.apiService.delete(`${this.GROUPS_PATH}/${id}`);
  }
}
