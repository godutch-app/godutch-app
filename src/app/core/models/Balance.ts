import { User } from "./User";

class UserBalance {
    user: User
    balance: number
}

class Payment {
    paidBy: User
    paidTo: User
    amount: number
}

export class Balance {
    balance: UserBalance[]
    payments: Payment[]
}
