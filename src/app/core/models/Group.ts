import { User } from './User';

export class Group {
    id: string
    name: string
    members: User[]
}