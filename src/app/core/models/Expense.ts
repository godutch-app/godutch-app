import { User } from './User';
import { Group } from './Group';

export class Expense {
    id: string
    description: string
    amount: number
    group: Group
    paidBy: User
    paidAt: Date
}