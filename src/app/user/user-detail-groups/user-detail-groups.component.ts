import { GroupService } from './../../core/services/group.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Group } from 'src/app/core/models/Group';
import {
  faTrashAlt,
  faEdit,
  faUser,
  IconDefinition,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-detail-groups',
  templateUrl: './user-detail-groups.component.html',
  styleUrls: ['./user-detail-groups.component.scss'],
})
export class UserDetailGroupsComponent implements OnInit {
  faTrashAlt: IconDefinition = faTrashAlt;
  faEdit: IconDefinition = faEdit;
  faUser: IconDefinition = faUser;

  @Input() userId: string;
  @Input() groups: Group[];
  @Output() refreshGroups = new EventEmitter<void>();

  constructor(private groupService: GroupService) {}

  ngOnInit(): void {}

  deleteGroup(e: PointerEvent, group: Group): void {
    e.preventDefault();
    this.groupService.delete(group.id).subscribe(() => this.refreshGroups.emit());
  }
}
