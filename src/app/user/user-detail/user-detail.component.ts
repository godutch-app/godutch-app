import { GroupService } from './../../core/services/group.service';
import { Group } from './../../core/models/Group';
import { User } from './../../core/models/User';
import { UserService } from './../../core/services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faTrashAlt, faEdit, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss'],
})
export class UserDetailComponent implements OnInit {
  faTrashAlt: IconDefinition = faTrashAlt;
  faEdit: IconDefinition = faEdit;

  id: string;
  user: User;
  groups: Group[];

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private groupService: GroupService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.getUser();
    });
  }

  getUser(): void {
    this.userService.findById(this.id).subscribe((res) => {
      this.user = res;
      this.getGroups();
    });
  }

  getGroups(): void {
    this.groupService.findByMember(this.user.id).subscribe((res) => {
      this.groups = res;
    });
  }

  deleteUser(e: PointerEvent, user: User): void {
    e.preventDefault();
    this.userService
      .delete(user.id)
      .subscribe(() => this.router.navigate(['/']));
  }
}
