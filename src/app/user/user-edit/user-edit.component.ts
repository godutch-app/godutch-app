import { UserService } from './../../core/services/user.service';
import { User } from './../../core/models/User';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
})
export class UserEditComponent implements OnInit {
  id: string;
  user: User;
  error: string = '';

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.getUser();
    });
  }

  getUser(): void {
    this.userService.findById(this.id).subscribe((res) => {
      this.user = res;
    });
  }

  onSubmit(): void {
    this.userService.update({ ...this.user }).subscribe(
      () => {
        this.router.navigate([`/users/${this.id}`]);
      },
      (err) => {
        this.error =
          err.errors && err.errors.length ? err.errors[0] : err.message;
      }
    );
  }
}
