import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'godutch-app';
  ready: boolean = false;
  retries: number = 0;
  maxRetries: number = 3;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.checkHealth();
  }

  checkHealth(): void {
    this.http.get(environment.healthUrl).subscribe(
      () => {
        this.ready = true;
      },
      () => {
        if (++this.retries <= this.maxRetries) {
          this.checkHealth();
        }
      }
    );
  }
}
