import { UserService } from './../../core/services/user.service';
import { ExpenseService } from './../../core/services/expense.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Expense } from './../../core/models/Expense';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-expense-edit',
  templateUrl: './expense-edit.component.html',
  styleUrls: ['./expense-edit.component.scss'],
})
export class ExpenseEditComponent implements OnInit {
  id: string;
  expense: Expense;
  referral: string;
  userEmail: string;
  error: string = '';

  constructor(
    private route: ActivatedRoute,
    private expenseService: ExpenseService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.getExpense();
    });
    this.route.queryParams.subscribe((params) => {
      this.referral = params.referral;
    });
  }

  getExpense(): void {
    this.expenseService.findById(this.id).subscribe((expense) => {
      this.expense = expense;
      this.userEmail = expense.paidBy.email;
    });
  }

  onSubmit(): void {
    this.userService.findByEmail(this.userEmail).subscribe(
      (paidBy) => {
        this.expenseService
          .update({ ...this.expense, paidBy })
          .subscribe(() => {
            this.router.navigate([`/groups/${this.expense.group.id}`]);
          });
      },
      (err) => {
        this.error =
          err.errors && err.errors.length ? err.errors[0] : err.message;
      }
    );
  }
}
