// MODULES
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// ROUTER
import { AppRoutingModule } from './app-routing.module';

// SERVICES
import { UserService } from './core/services/user.service';

// COMPONENTS
import { AppComponent } from './app.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { GroupCreateComponent } from './group/group-create/group-create.component';
import { GroupDetailComponent } from './group/group-detail/group-detail.component';
import { ExpenseCreateComponent } from './expense/expense-create/expense-create.component';
import { GroupEditComponent } from './group/group-edit/group-edit.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { ExpenseEditComponent } from './expense/expense-edit/expense-edit.component';
import { GroupDetailMembersComponent } from './group/group-detail-members/group-detail-members.component';
import { GroupDetailExpensesComponent } from './group/group-detail-expenses/group-detail-expenses.component';
import { GroupDetailBalanceComponent } from './group/group-detail-balance/group-detail-balance.component';
import { UserDetailGroupsComponent } from './user/user-detail-groups/user-detail-groups.component';

registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    UserCreateComponent,
    UserLoginComponent,
    UserDetailComponent,
    GroupCreateComponent,
    GroupDetailComponent,
    ExpenseCreateComponent,
    GroupEditComponent,
    UserEditComponent,
    ExpenseEditComponent,
    GroupDetailMembersComponent,
    GroupDetailExpensesComponent,
    GroupDetailBalanceComponent,
    UserDetailGroupsComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, FontAwesomeModule],
  providers: [{ provide: LOCALE_ID, useValue: 'es' }, UserService],
  bootstrap: [AppComponent],
})
export class AppModule {}
