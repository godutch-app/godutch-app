export const environment = {
  production: true,
  apiUrl: "https://godutch-api.herokuapp.com/api",
  healthUrl: "https://godutch-api.herokuapp.com/health"
};
